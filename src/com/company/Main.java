package com.company;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        System.out.println("Введите количество строк: ");
        int kolStrok = new Scanner(System.in).nextInt();
        System.out.println("Введите количество букв, которое нужно удалить: ");
        int kolBukv = new Scanner(System.in).nextInt();
        String[] arr = new String[kolStrok];
        for (int i = 0; i < kolStrok; i++) {
            System.out.println("Введите " + (i + 1) + "ю строку");
            arr[i] = new Scanner(System.in).nextLine();
        }
        for (int j = 0; j < kolStrok; j++) {
            String[] arrOfWords = arr[j].split(" ");
            StringBuilder strBuild = new StringBuilder();
            for (int n = 0; n < arrOfWords.length; n++) {
                char firstBukva = arrOfWords[n].charAt(0);
                if (!((arrOfWords[n].length() == kolBukv ) && ((firstBukva != 'а') && (firstBukva != 'у') && (firstBukva != 'и') && (firstBukva != 'о') && (firstBukva != 'ё') && (firstBukva != 'ы') && (firstBukva != 'ю') && (firstBukva != 'е') && (firstBukva != 'э') && (firstBukva != 'я') && (firstBukva != 'А') && (firstBukva != 'У') && (firstBukva != 'И') && (firstBukva != 'О') && (firstBukva != 'Ё') && (firstBukva != 'Ы') && (firstBukva != 'Ю') && (firstBukva != 'Е') && (firstBukva != 'Э') && (firstBukva != 'Я')))) {
                    strBuild.append(arrOfWords[n]).append(" ");
                }
            }
            String vivodText = strBuild.toString().trim();
            System.out.print((j + 1) + "я строка: " + vivodText + "\n");
        }
    }
}